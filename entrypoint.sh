#!/bin/sh

# Enable eureka functionality if the eureka url is set
if [ ! -z "${EUREKA_URL}"]; then
    export eureka__enabled=true
    export eureka__client__serviceUrl=${EUREKA_URL}:${EUREKA_PORT}/eureka
    export eureka__instance__hostname=${SELF_DOMAIN}
    export eureka__instance__port=9000

    while ! nc -z ${EUREKA_URL} ${EUREKA_PORT} ; do
        echo "Archive Service waiting for upcoming eureka service"
    done
fi

# Set url of authentication service
export Jwt__Authority=${AUTH_URL}

printenv > .used_env
dotnet ./ProSkive.Services.Archive.dll