﻿using ProSkive.Lib.MongoDb.Settings;

namespace ProSkive.Services.Archive
{
    public class Appsettings
    {
        public MongoDbSettings MongoDb { get; set; }
    }
}