﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProSkive.Services.Archive.Extensions;

namespace ProSkive.Services.Archive.Helpers
{
    public static class DictionaryHelper
    {
        public static List<object> ToDictionaryRecursive(List<object> list)
        {
            var dictList = new List<object>();

            foreach (var element in list)
            {
                if (element is List<object> listElement)
                {
                    dictList.Add(ToDictionaryRecursive(listElement));
                }
                else
                {
                    dictList.Add(ToDictionary(element));
                }
            }
            
            return dictList;
        }

        public static object ToDictionary(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            //return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            return JToken.Parse(json).ToObjectRecursive();
        }
    }
}