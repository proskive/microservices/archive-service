﻿﻿using System;
using System.Collections.Generic;

namespace ProSkive.Services.Archive.Resources.V1
{
    
    public class ArchiveInstance
    {
        public int Version { get; set; }
        public object Data { get; set; }
        public DateTime Created { get; set; }
        public List<string> Diff { get; set; }
    }
}