﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ProSkive.Services.Archive.Resources.V1
{
    
    public class ArchiveResource
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Hash { get; set; }
        public List<ArchiveInstance> Instances { get; set; }
    }
}