﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProSkive.Lib.Common.Extensions;
using ProSkive.Lib.MongoDb.Contributors.Health;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Archive.Repositories;
using Steeltoe.Management.Endpoint.Health;
using Swashbuckle.AspNetCore.Swagger;

namespace ProSkive.Services.Archive
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Appsettings>(Configuration);
            services.Configure<MongoOptionsSnapshot>(Configuration);
            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });
         
            services.AddEureka(Configuration);
            services.AddJwt(Configuration);
            services.AddHealthAndInfoEndpoints(Configuration);

            services.AddCors();
            services.AddRouting(options => { options.LowercaseUrls = true; });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            // Dependency Injection
            services.AddScoped<IArchiveRepository, ArchiveRepository>();
            
            // Health Contributors
            services.AddScoped<IHealthContributor, MongoHealthContributor>();
            
            // Health and Info Endpoints
            services.AddHealthAndInfoEndpoints(Configuration);
            
            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ProSkive Archive API", Version = "v1" });
                
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Auth
            app.UseAuthentication();
            
            // Cors
            app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            
            // Swagger
            app.UseSwagger();

            // Swagger UI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProSkive Archive API v1");
            });

            // MVC Middleware
            app.UseMvc();
            
            // Use Eureka
            app.UseEureka(Configuration);
            
            //Health Points
            app.UseHealthAndInfoEndpoints();
            
            // Management
            app.UseHealthAndInfoEndpoints();
        }
    }
}
