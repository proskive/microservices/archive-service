﻿﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using KellermanSoftware.CompareNetObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json.Linq;
using ProSkive.Lib.Api.Resources.Errors;
using ProSkive.Lib.MongoDb.Settings;
using ProSkive.Lib.Utils.Comparer;
using ProSkive.Services.Archive.Extensions;
using ProSkive.Services.Archive.Helpers;
using ProSkive.Services.Archive.Repositories;
using ProSkive.Services.Archive.Resources.V1;

namespace ProSkive.Services.Archive.Controllers.V1
{
    [Authorize]
    [ApiController]
    [Route("api/v1/archive")]
    public class ArchiveController : ControllerBase
    {
   
        public MongoDbSettings Settings { get; set; }

        private readonly IArchiveRepository repository;
        
        public ArchiveController(IOptionsSnapshot<Appsettings> settings, IArchiveRepository repository)
        {
            this.repository = repository;
            Settings = settings.Value.MongoDb;
        }
        
        // GET api/values
        /// <summary>
        /// Retrieves all archives.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ArchiveResource>>> Get()
        {
            var entries = await repository.GetAllArchivesAsync();
            return entries;
        }

        // GET api/values/5
        /// <summary>
        /// Retrieves an archive with a specific hash.
        /// </summary>
        [HttpGet("{hash}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<ArchiveResource>> Get(string hash)
        {
            if (string.IsNullOrWhiteSpace(hash))
            {
                return BadRequest(new BadRequestApiResource("Invalid hash"));
            }
            
            var entry = await repository.GetArchivesForHashAsync(hash);
            if (entry == null)
            {
                return NotFound(new ResourceNotFoundApiResource("Archive could not be found"));
            }
            
            return entry;
        }
        
        /// <summary>
        /// Get count of all versions of an archive.
        /// </summary>
        [HttpGet("{hash}/count")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<int>> GetCount(string hash)
        {
            if (string.IsNullOrWhiteSpace(hash))
            {
                return BadRequest(new BadRequestApiResource("Invalid hash"));
            }
            
            var instanceCount = await repository.GetNumberOfInstancesAsync(hash);
            if (instanceCount < 0)
            {
                return NotFound(new ResourceNotFoundApiResource("Archive could not be found"));
            }
            
            return instanceCount;
        }

        /// <summary>
        /// Retrieves a specific version of an archive.
        /// </summary>
        [HttpGet("{hash}/{version}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<ArchiveInstance>> GetSpecificVersion(string hash, int version)
        {
            var entry = await repository.GetArchivesForHashAsync(hash);
           
            if (entry == null)
            {
                return NotFound(new ResourceNotFoundApiResource("Archive could not be found"));
            }

            var instance = repository.GetSpecificInstance(version, entry);
            
            if (instance == null)
            {
                return NotFound(new ResourceNotFoundApiResource("Version could not be found"));
            }
            
            return instance;
        }

        // POST api/values
        /// <summary>
        /// Saves an archive for a specific hash.
        /// </summary>
        [HttpPost("{hash}", Name = RouteNames.GetArchiveRoute)]
        [ProducesResponseType(400)]
        [ProducesResponseType(201)]
        public async Task<ActionResult<ArchiveInstance>> SaveArchive (string hash, JObject data)
        {
            if (!ModelState.IsValid)
                return BadRequest(new BadRequestApiResource(ModelState));
            
            
            // 1. In MongoDB nachschauen, ob Archiv mit hash existiert und return
            var resource = await repository.GetNumberOfInstancesAsync(hash);
            // Create instance
            var instance = new ArchiveInstance()
            {
                Version = 1,
                Created = DateTime.Now,
                Data = BsonSerializer.Deserialize<object>(data.ToString()),
                Diff = null
            };

            ArchiveResource archiveResource = null;
            if (resource < 1)
            {
                archiveResource = new ArchiveResource
                {
                    Hash = hash,
                    Instances = new List<ArchiveInstance>
                    {
                        instance
                    }
                    
                };
                await repository.SaveOneArchiveAsync(archiveResource);
            }
            else
            {
                archiveResource = await repository.GetArchivesForHashAsync(hash);

                if (archiveResource != null)
                {
                    var numOfInstances = archiveResource.Instances.Count;
                
                    // Update Version
                    instance.Version = numOfInstances + 1;
                    
                    // Create Diff
                    // -- Previous Data
                    var previousInstance = archiveResource.Instances[numOfInstances - 1];
                    var previousData = (ExpandoObject) previousInstance.Data;
                    var previousDataDictionary = (IDictionary<string, object>) previousData;

                    // -- Differing Fields
                    var differingFields = new List<string>();

                    foreach (var dataField in data)
                    {
                        if (!previousDataDictionary.ContainsKey(dataField.Key))
                        {
                            differingFields.Add(dataField.Key);
                            continue;
                        }

                        var success = previousDataDictionary.TryGetValue(dataField.Key, out var previousFieldValue);
                        if (!success)
                        {
                            differingFields.Add(dataField.Key);
                        }

                        var compareLogic = new CompareLogic();
                        compareLogic.Config.CustomComparers.Add(new ExpandoObjectComparer(RootComparerFactory.GetRootComparer()));
                        
                        ComparisonResult result = null;
                        
                        switch (dataField.Value.Type)
                        {
                            case JTokenType.String:
                                result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<string>());
                                break;
                            case JTokenType.Array:
                                if (previousFieldValue?.GetType() != typeof(List<object>))
                                    break;
                                var previousList = DictionaryHelper.ToDictionaryRecursive((List<object>) previousFieldValue);
                                result = compareLogic.Compare(previousList, dataField.Value.ToObjectRecursive());
                                break;
                            case JTokenType.Boolean:
                                result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<bool>());
                                break;
                            case JTokenType.Float:
                                if (previousFieldValue?.GetType() == typeof(float))
                                    result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<float>());
                                else
                                    result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<double>());

                                break;
                            case JTokenType.Integer:
                                if (previousFieldValue?.GetType() == typeof(int))
                                    result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<int>());
                                else
                                    result = compareLogic.Compare(previousFieldValue, dataField.Value.ToObject<long>());

                                break;
                            case JTokenType.Object:
                                if (!(previousFieldValue is ExpandoObject))
                                    break;

                                result = compareLogic.Compare(DictionaryHelper.ToDictionary(previousFieldValue), dataField.Value.ToObjectRecursive());
                                
                                break;
                            case JTokenType.Null:
                                result = compareLogic.Compare(previousFieldValue, null);
                                break;
                            default:
                                result = null;
                                break;
                        }
                        
                        if (result == null || !result.AreEqual)
                            differingFields.Add(dataField.Key);
                    }

                    instance.Diff = differingFields;
                }

                await repository.UpdateArchiveAsync(instance, hash);
            }
            
            // 2a. Falls existiert, Resource anhängen an Instances
            // 2b. Falls nicht existiert, dann neues Archiv erstellen und Instances mit einem Element erstellen
            return CreatedAtRoute(RouteNames.GetArchiveRoute, new { hash = archiveResource?.Hash }, instance);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
