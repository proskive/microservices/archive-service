﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using ProSkive.Lib.MongoDb.Exceptions;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Archive.Resources.V1;
using MongoUtils = ProSkive.Lib.MongoDb.MongoUtils;

namespace ProSkive.Services.Archive.Repositories
{
    public class ArchiveRepository : IArchiveRepository
    {
        
        private readonly MongoOptionsSnapshot settings;
        private readonly ILogger<ArchiveRepository> logger;

        private IMongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<ArchiveResource> collection;
        
        public ArchiveRepository(ILogger<ArchiveRepository> logger, IOptionsSnapshot<MongoOptionsSnapshot> settings)
        {
            this.logger = logger;
            this.settings = settings.Value;
            
            var connectionEstablished = MongoUtils.ConnectToCollection<ArchiveResource>(
                this.settings.MongoDb,
                out var collection,
                out var client,
                out var database
            );

            if (!connectionEstablished)
                logger.LogError("Could not connect to collection or even database");

            this.client = client;
            this.database = database;
            this.collection = collection;
        }
      
        public async Task<List<ArchiveResource>> GetAllArchivesAsync()
        {
            if (collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            return await collection.Find(_ => true).ToListAsync();
        }

        public async Task<ArchiveResource> GetArchivesForHashAsync(string hash)
        {
            if (collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);

            if (await MongoUtils.IsCollectionEmptyAsync(collection))
                return null;
            
            var entryList = await collection.Find(archive => archive.Hash.Equals(hash)).ToListAsync();
            return entryList.FirstOrDefault();
        }

        public async Task<int> GetNumberOfInstancesAsync(string hash)
        {
            if (collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            var entries = await collection.Find(archive => archive.Hash.Equals(hash)).ToListAsync();
            var entry = entries?.FirstOrDefault();
            
            if (entry == null) 
                return -1;
            
            var instanceCount = entry.Instances.Count;
            return instanceCount;

        }

        public ArchiveInstance GetSpecificInstance(int version, ArchiveResource archiveResource)
        {
            return archiveResource.Instances.AsQueryable<ArchiveInstance>()
                .FirstOrDefault(inst => inst.Version == version);
        }

        public async Task SaveOneArchiveAsync(ArchiveResource archiveResource)
        {
            if (collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            await collection.InsertOneAsync(archiveResource);
        }

        public async Task UpdateArchiveAsync(ArchiveInstance instance, string hash)
        {
            if (collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            var updateDefinition = new UpdateDefinitionBuilder<ArchiveResource>()
                .Push(n => n.Instances, instance);
            await collection.UpdateOneAsync(x => x.Hash.Equals(hash), updateDefinition);
        }
    }
}