﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProSkive.Services.Archive.Resources.V1;

namespace ProSkive.Services.Archive.Repositories
{
    public interface IArchiveRepository
    {
        Task<List<ArchiveResource>> GetAllArchivesAsync();
        Task<ArchiveResource> GetArchivesForHashAsync(string hash);
        Task<int> GetNumberOfInstancesAsync(string hash);
        ArchiveInstance GetSpecificInstance(int version, ArchiveResource archiveResource);
        Task SaveOneArchiveAsync(ArchiveResource archiveResource);
        Task UpdateArchiveAsync(ArchiveInstance instance, string hash);
    }
}