﻿using System.Linq;
using Newtonsoft.Json.Linq;

namespace ProSkive.Services.Archive.Extensions
{
    public static class JTokenExtensions
    {
        public static object ToObjectRecursive(this JToken token)
        {
            switch (token.Type)
            {
                case JTokenType.Object:
                    return token.Children<JProperty>()
                        .ToDictionary(prop => prop.Name,
                           prop => ToObjectRecursive(prop.Value));

                case JTokenType.Array:
                    return token.Select(ToObjectRecursive).ToList();

                default:
                    return ((JValue)token).Value;
            }
        }
    }
}