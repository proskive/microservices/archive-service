﻿using System.Dynamic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using ProSkive.Clients.Archive.V1;
using ProSkive.Clients.Archive.V1.Resources;
using ProSkive.Lib.MongoDb.Helpers;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Lib.TestsCommon;
using ProSkive.Lib.TestsCommon.Http;
using ProSkive.Services.Archive;
using ProSkive.Services.Archive.Resources.V1;

namespace ProSkive.Tests.Services.Archive.IntegrationTests.RepositoryTests
{
    [TestFixture]
    [Category("Integration"), Category("Repositories"), Category("ArchiveRepository")]
    public class ArchiveRepositoryTest
    {
        private TestServer server;
        private ArchivesFlurlClient client;
        private MongoTestHelper<ArchiveResource> mongoHelper;

        [OneTimeSetUp]
        public void Init()
        {          
            // Test Server
            server = TestServerHelper.CreateTestServer<TestStartup>(TestEnv.Get());

            // Flurl Factory Config
            FlurlTestHelper.UseInMemoryServer(server);
        }

        [OneTimeTearDown]
        public void Finish()
        {
            // Reset Flurl
            FlurlTestHelper.ResetClientFactory();
            
            // Dispose Server
            server.Dispose();
        }

        [SetUp]
        public void SetUp()
        {
            // Mongo
            if (server.Host.Services.GetService(typeof(IOptionsSnapshot<MongoOptionsSnapshot>)) is IOptionsSnapshot<MongoOptionsSnapshot> mongoSettings)
            {
                mongoHelper = new MongoTestHelper<ArchiveResource>(mongoSettings.Value.MongoDb);
                mongoHelper.CleanCollection();
            }
            
            // Flurl
            client = new ArchivesFlurlClient("http://localhost");
        }

        public async Task<ArchiveInstanceApiResource> AddArchiveDummy(string hash, string token)
        {
            return await client.SaveArchiveAsync(hash, new { Dummy = "Data" }, token);
        }

        [Test]
        public async Task FindAllArchives()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            await AddArchiveDummy("hash", "token");
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
            
            // Act
            var result = await client.GetAllArchivesAsync();
            
            // Assert
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }

        [Test]
        public async Task FindArchiveForHash()
        {
            const string hash = "hash";
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            await AddArchiveDummy(hash, "token");
            await AddArchiveDummy(hash, "token");
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
            
            // Act 
            var result = await client.GetArchiveAsync(hash);
            
            // Assert
            Assert.That(result.Instances.Count, Is.EqualTo(2));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task GetArchiveCount()
        {
            const string hash = "hash";
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            await AddArchiveDummy(hash, "token");
            await AddArchiveDummy("another", "token");
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(2));
            
            // Act 
            var result = await client.GetCountArchiveAsync(hash);
            
            // Assert
            Assert.That(result, Is.EqualTo(1));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(2));
        }
        
        [Test]
        public async Task FindSpecificVersion()
        {
            const string hash = "hash";
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            await AddArchiveDummy(hash, "token");
            await AddArchiveDummy(hash, "token");
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
            
            // Act 
            var result = await client.GetSpecificVersionArchiveAsync(hash, 2);
            
            // Assert
            Assert.That(result.Version, Is.EqualTo(2));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task SaveArchive()
        {
            const string hash = "hash";
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));

            dynamic dataInstance1 = new ExpandoObject();
            dataInstance1.Name = "testMe";
            var data = (ExpandoObject) dataInstance1;
            
            // Act 
            await client.SaveArchiveAsync(hash, data);
            
            // Assert
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }

    }
}