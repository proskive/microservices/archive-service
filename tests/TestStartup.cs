﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProSkive.Lib.Common.Extensions;
using ProSkive.Lib.MongoDb.Contributors.Health;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Archive.Repositories;
using ProSkive.Lib.TestsCommon.Auth.FakeJwt;
using Steeltoe.Management.Endpoint.Health;

namespace ProSkive.Services.Archive
{
    public class TestStartup
    {
        public TestStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Appsettings>(Configuration);
            services.Configure<MongoOptionsSnapshot>(Configuration);
         
            services.AddEureka(Configuration);
            //services.AddJwt(Configuration);
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = FakeJwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = FakeJwtBearerDefaults.AuthenticationScheme;
                })
                .AddFakeJwtBearer();
            services.AddHealthAndInfoEndpoints(Configuration);

            services.AddCors();
            services.AddRouting(options => { options.LowercaseUrls = true; });
            services.AddMvc();
            
            // Dependency Injection
            services.AddScoped<IArchiveRepository, ArchiveRepository>();
            
            // Health Contributors
            services.AddScoped<IHealthContributor, MongoHealthContributor>();
            
            // Health and Info Endpoints
            services.AddHealthAndInfoEndpoints(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Auth
            app.UseAuthentication();
            
            //Cors
            app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });

            // MVC Middleware
            app.UseMvc();
            
            // Use Eureka
            app.UseEureka(Configuration);
            
            //Health Points
            app.UseHealthAndInfoEndpoints();
            
            // Management
            app.UseHealthAndInfoEndpoints();
        }
    }
}
