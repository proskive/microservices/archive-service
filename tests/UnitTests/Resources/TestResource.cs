﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using ProSkive.Services.Archive.Resources.V1;

namespace ProSkive.Tests.Services.Archive.UnitTests.Resources
{
    public class TestResource
    {
        
        public ArchiveResource CreateFakeResource(string hash)
        {

            ArchiveResource archiveResource = null;
            
            if (hash.Equals("existent_hash"))
            {
                // Data 1
                dynamic dataInstance1 = new ExpandoObject();
                dataInstance1.Name = "Barack Obama";
                
                // Data 2
                dynamic dataInstance2 = new ExpandoObject();
                dataInstance2.Name = "Donald Trump";
                
                archiveResource = new ArchiveResource()
                {
                    Hash = hash,
                    Id = "id2",
                    Instances = new List<ArchiveInstance>()
                    {
                        new ArchiveInstance()
                        {
                            Created = DateTime.Now,
                            Data = dataInstance1,
                            Version = 1

                        },
                        new ArchiveInstance()
                        {
                            Created = DateTime.Now,
                            Data = dataInstance2,
                            Version = 2
                        }
                    }

                };
            }

            return archiveResource;
        }

        public List<ArchiveResource> CreateFakeResourceList()
        {
            var archiveResources = new List<ArchiveResource>()
            {
                new ArchiveResource()
                {
                    Id = "oihfp1",
                    Hash = "hash1",
                    Instances = new List<ArchiveInstance>()
                    {
                        new ArchiveInstance()
                        {
                            Created = DateTime.Now,
                            Data = new
                            {
                                Name = "Patric"
                            },
                            Version = 1234

                        }
                    }
                },
                new ArchiveResource()
                {
                    Id = "oihfp2",
                    Hash = "hash2",
                    Instances = new List<ArchiveInstance>()
                    {
                        new ArchiveInstance()
                        {
                            Created = DateTime.Now,
                            Data = new
                            {
                                Name = "Timo"
                            },
                            Version = 1337

                        }
                    }
                }
            };
            return archiveResources;

        }
        
    }
}