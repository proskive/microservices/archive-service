﻿using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using ProSkive.Services.Archive;
using ProSkive.Services.Archive.Controllers.V1;
using ProSkive.Services.Archive.Repositories;
using ProSkive.Services.Archive.Resources.V1;

namespace ProSkive.Tests.Services.Archive.UnitTests.Controllers.V1
{
    
    
    [TestFixture]
    [Category("Unit"), Category("Controller"), Category("ArchiveController")]
    public class ArchiveControllerSaveArchiveTests
    {
        public Mock<IOptionsSnapshot<Appsettings>> MockSettings { get; set; }
        
        [SetUp]
        public void SetUp()
        {
            var appSettings = new Appsettings();
            MockSettings = new Mock<IOptionsSnapshot<Appsettings>>();
            MockSettings.Setup(snapshot => snapshot.Value).Returns(appSettings);
            
        }

        #region Type: String

        [Test]
        public void SaveArchive_OnSameString_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""str"": ""Hello World!""
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.str = "Hello World!";
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentString_HasDiff()
        {
            // Body
            const string json = @"{
                ""str"": ""Different str!""
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.str = "Hello World!";
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Array

        [Test]
        public void SaveArchive_OnSameArray_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""arr"": [
                    ""random string one"",
                    ""random string two""
                ]
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.arr = new List<object>()
            {
                "random string one",
                "random string two"
            };
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentArray_HasDiff()
        {
            // Body
            const string json = @"{
                ""arr"": [
                    ""random string one""
                ]
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.arr = new List<object>()
            {
                "random string one",
                "random string two"
            };
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Bool

        [Test]
        public void SaveArchive_OnSameBoolean_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""boo"": true
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.boo = true;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentBoolean_HasDiff()
        {
            // Body
            const string json = @"{
                ""boo"": false
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.boo = true;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Float

        [Test]
        public void SaveArchive_OnSameFloat_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""flo"": 2.45678
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.flo = 2.45678f;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentFloat_HasDiff()
        {
            // Body
            const string json = @"{
                ""flo"": ""str""
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.flo = 2.45678f;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Double

        [Test]
        public void SaveArchive_OnSameDouble_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""dou"": 2.4567890123
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.dou = 2.4567890123;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentDouble_HasDiff()
        {
            // Body
            const string json = @"{
                ""dou"": ""str""
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.dou = 2.4567890123;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }


        #endregion
        
        
        #region Type: Integer

        [Test]
        public void SaveArchive_OnSameInteger_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""integ"": 1
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.integ = 1;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentInteger_HasDiff()
        {
            // Body
            const string json = @"{
                ""integ"": 99999
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.integ = 1;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Long

        [Test]
        public void SaveArchive_OnSameLong_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""lo"": 1000000
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.lo = 1000000L;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentLong_HasDiff()
        {
            // Body
            const string json = @"{
                ""lo"": -500
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.lo = 1000000L;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Object

        [Test]
        public void SaveArchive_OnSameObject_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""obj"": {
                    ""str"": ""Hello!""
                }
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.obj = new ExpandoObject();
            expandoData.obj.str = "Hello!";
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentObject_HasDiff()
        {
            // Body
            const string json = @"{
                ""obj"": {
                    ""other"": 1
                }
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.obj = new ExpandoObject();
            expandoData.obj.str = "Hello!";
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }

        #endregion
        
        

        #region Type: Null
        
        [Test]
        public void SaveArchive_OnSameNull_IsEmptyDiff()
        {
            // Body
            const string json = @"{
                ""nu"": null
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.nu = null;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(0, newInstance.Diff.Count);
        }
        
        [Test]
        public void SaveArchive_OnDifferentNull_HasDiff()
        {
            // Body
            const string json = @"{
                ""nu"": ""str""
            }";
            
            // Previous
            dynamic expandoData = new ExpandoObject();
            expandoData.nu = null;
            
            var archiveResource = new ArchiveResource()
            {
                Instances = new List<ArchiveInstance>()
                {
                    new ArchiveInstance()
                    {
                        Data = expandoData
                    }
                }
            };
            
            var mockRepo = new Mock<IArchiveRepository>();
            mockRepo.Setup(repo => repo.GetNumberOfInstancesAsync("hash"))
                .Returns(() => Task.FromResult(archiveResource.Instances.Count));
            mockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), "hash")).Returns(Task.CompletedTask);
            mockRepo.Setup(repo => repo.GetArchivesForHashAsync("hash")).Returns(() => Task.FromResult(archiveResource));

            var archiveController = new ArchiveController(MockSettings.Object, mockRepo.Object);
            
            // Act
            var result = archiveController.SaveArchive("hash", JObject.Parse(json));
            
            // Assert
            var createdAt = (CreatedAtRouteResult) result.Result.Result;
            var newInstance = (ArchiveInstance) createdAt.Value;
            Assert.AreEqual(1, newInstance.Diff.Count);
        }
        
        #endregion
    }
}