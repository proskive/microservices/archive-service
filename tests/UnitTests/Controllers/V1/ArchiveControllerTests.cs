﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using ProSkive.Lib.MongoDb.Settings;
using ProSkive.Services.Archive;
using ProSkive.Services.Archive.Controllers.V1;
using ProSkive.Services.Archive.Repositories;
using ProSkive.Services.Archive.Resources.V1;
using ProSkive.Tests.Services.Archive.UnitTests.Resources;

namespace ProSkive.Tests.Services.Archive.UnitTests.Controllers.V1
{
    [TestFixture]
    [Category("Unit"), Category("Controller"), Category("ArchiveController")]
    public class ArchiveControllerTests
    {
        public Mock<IOptionsSnapshot<Appsettings>> MockSettings { get; set; }
        public Mock<IArchiveRepository> MockRepo { get; set; }

        [SetUp]
        public void SetUp()
        {
            var appSettings = new Appsettings()
            {
                MongoDb = new MongoDbSettings()
                {
                    CollectionName = "Latte",
                    Database = "völlig",
                    ConnectionString = "richtig"
                }
            };
            
            MockSettings = new Mock<IOptionsSnapshot<Appsettings>>();
            MockSettings.Setup(snapshot => snapshot.Value).Returns(appSettings);
            
        }
        
        [Test]
        public async Task Get_WhenCalled_RespondWith200Ok()
        {
            // Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetAllArchivesAsync())
                .Returns(() => Task.FromResult(new TestResource().CreateFakeResourceList()));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);

            // Act
            var result = await archiveController.Get();
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var archiveResources = result.Value;
            Assert.That(archiveResources.Count, Is.EqualTo(2));
        }

        [Test]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        public async Task GetWithHash_WithBadInput_RespondWith400BadRequest(string hash)
        {
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash))
                .Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.Get(hash);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());

        }
        
        [Test]
        [TestCase("non_existent")]
        public async Task GetWithHash_WithNonExistentHash_RespondWith404NotFound(string hash)
        {
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash))
                .Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.Get(hash);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundObjectResult>());
        }

        [Test]
        [TestCase("existent_hash")]
        public async Task GetWithHash_WhenCalled_RespondWith200Ok(string hash) {
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash))
                .Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.Get(hash);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

        }
        
        [Test]
        [TestCase("existent_hash")]
        public async Task GetCount_WhenCalled_RespondWith200Ok(string hash) {
            
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetNumberOfInstancesAsync(hash))
                .Returns(() => Task.FromResult(new int()));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.GetCount(hash);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

        }

        [Test]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        public async Task GetCount_WithBadInput_RespondWith400BadRequest(string hash)
        {
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetNumberOfInstancesAsync(hash))
                .Returns(() => Task.FromResult(new int()));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.GetCount(hash);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }
        
        [Test]
        [TestCase("non_existent")]
        public async Task GetCount_WithNonExistentHash_RespondWith404NotFound(string hash)
        {
            //Arrange
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash))
                .Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));
            
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.Get(hash);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundObjectResult>());
        }
        
        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public async Task SaveArchive_WhenCalled_RespondWith201Created(int numOfInstances) {
            
            //Arrange
            const string hash = "existent_hash";
            var data = JObject.Parse(@"{
               'name': 'Test JSON'
            }");
            
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetNumberOfInstancesAsync(hash))
                .Returns(() => Task.FromResult(numOfInstances));
            MockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>())).Returns(Task.CompletedTask);
            MockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), hash)).Returns(Task.CompletedTask);
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash)).Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));

           
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            
            // Act
            var result = await archiveController.SaveArchive(hash, data);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<CreatedAtRouteResult>());

            var craetedResult = (ObjectResult) result.Result;
            Assert.That(craetedResult.Value, Is.TypeOf<ArchiveInstance>());
            
            
        }
        
        [Test]
        [TestCase(0)]
        [TestCase(1)]
        public async Task SaveArchive_WhenCalled_RespondWith400BadRequest(int numOfInstances) {
            
            //Arrange
            const string hash = "existent_hash";
            var data = JObject.Parse(@"{
               'name': 'Test JSON'
            }");
            
            MockRepo = new Mock<IArchiveRepository>();
            MockRepo.Setup(repo => repo.GetNumberOfInstancesAsync(hash))
                .Returns(() => Task.FromResult(numOfInstances));
            MockRepo.Setup(repo => repo.SaveOneArchiveAsync(It.IsAny<ArchiveResource>()));
            MockRepo.Setup(repo => repo.UpdateArchiveAsync(It.IsAny<ArchiveInstance>(), hash));
            MockRepo.Setup(repo => repo.GetArchivesForHashAsync(hash)).Returns(() => Task.FromResult(new TestResource().CreateFakeResource(hash)));

         
            var archiveController = new ArchiveController(MockSettings.Object, MockRepo.Object);
            archiveController.ModelState.AddModelError("", "Error");
            
            // Act
            var result = await archiveController.SaveArchive(hash, data);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());

        }
        
    }
}